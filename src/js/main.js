$(document).ready(function () {

    // Navigation toggle
    $('.js-toggle').click(function (e) {
        e.preventDefault();
        $('.c-nav').toggleClass('active');
        $('.c-nav-mobile-toggle').toggleClass('active');
    });

    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        }
    });

});