var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    plumber = require('gulp-plumber'),
    util = require('gulp-util'),
    del = require('del');

gulp.task('vendor', function() {
  del.sync(settings.vendor.dist);
  gulp.src(settings.vendor.src + '**/*')
    .pipe(plumber(function(error) {
      util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
      this.emit('end');
    }))
  .pipe(gulp.dest(settings.vendor.dist))
  .pipe(browserSync.reload({
    stream: true
  }));
});