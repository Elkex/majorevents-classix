var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    imagemin = require('gulp-imagemin'),
    gulpif = require('gulp-if'),
    plumber = require('gulp-plumber'),
    util = require('gulp-util'),
    del = require('del');

gulp.task('images', function() {
  del.sync(settings.images.dist);
  gulp.src(settings.images.src + '**/*.{jpeg,jpg,png,gif,svg}')
    .pipe(plumber(function(error) {
      util.log(util.colors.red('Error (' + error.plugin + '): ' + error.message));
      this.emit('end');
    }))
    .pipe(gulpif(settings.production, imagemin([
      imagemin.gifsicle({
        interlaced: true
      }),
      imagemin.jpegtran({
        progressive: true
      }),
      imagemin.optipng({
        optimizationLevel: 5
      }),
      imagemin.svgo({
        plugins: [{
          removeViewBox: true
        }]
      })
    ])))
  .pipe(gulp.dest(settings.images.dist))
  .pipe(browserSync.reload({
    stream: true
  }));
});